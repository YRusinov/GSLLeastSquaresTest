#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include <math.h>

#include <gsl/gsl_fit.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_randist.h>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::stringstream;

int main (void)
{
    cout << "This is test of gsl linear squares" << endl;
    double xMin;
    double xMax;

//    double yMin;
//    double yMax;

    int nPol (0);
    cout << "Please enter polynomial power" << endl;
    cin >> nPol;
    if (nPol < 0)
    {
        cout << "There is need positive power";
        return 1;
    }
    cout << nPol << endl;

    cout << "Please enter variable range" << endl;
    cin >> xMin;
    cin >> xMax;
    if (xMin > xMax)
    {
        double t = xMin;
        xMin = xMax;
        xMax = t;
    }
    cout << xMin << " " << xMax << endl;
    int n;
    cout << "Enter number of variables" << endl;
    cin >> n;
    cout << n << endl;
    double dx = (xMax-xMin)/n;

    gsl_matrix *XMatr, *covMatr;
    gsl_vector * c;
    gsl_vector * yVec;
    double * coeff0 = new double [nPol+1];
    cout << "Enter polynomial coefficients " << endl;
    for (int i=0; i<=nPol;i++)
        cin >> coeff0[i];

    for (int i=0; i<= nPol; i++)
        cout << coeff0[i] << endl;

    cout << "Enter noise dispersion" << endl;
    double sigma;
    cin >> sigma;
    cout << sigma << endl;
    const gsl_rng_type * T;
    gsl_rng * r;
    gsl_rng_env_setup ();
    double * y = new double [n];
    XMatr = gsl_matrix_alloc (n, nPol+1);
    covMatr = gsl_matrix_alloc (nPol+1, nPol+1);
    yVec = gsl_vector_alloc (n);
    T = gsl_rng_default;
    r = gsl_rng_alloc (T);
    for (int i=0; i<n; i++)
    {
        double x = xMin+dx*i;
        double val = coeff0[0];
        for (int j=1; j<=nPol; j++)
        {
            val = val*x+coeff0[j];
        }
        double dy = gsl_ran_gaussian (r, sigma);
        y[i] = val+dy;
        for (int j=0; j<=nPol; j++)
        {
            gsl_matrix_set (XMatr, i, j, pow (x, nPol-j));
        }
        gsl_vector_set (yVec, i, val);
    }
    gsl_rng_free(r);
    cout << "Source matrix is " << endl;
    for (int i=0; i<n; i++)
    {
        stringstream wStr;
        for (int j=0; j<=nPol; j++)
            wStr << gsl_matrix_get(XMatr,(i),(j)) << " ";
        wStr << " " << y[i];
        cout << wStr.str() << endl;
    }
    cout << endl;

    c = gsl_vector_alloc (nPol+1);
    double chi2;
    gsl_multifit_linear_workspace * work = gsl_multifit_linear_alloc (n, nPol+1);
    gsl_multifit_linear (XMatr, yVec, c, covMatr,
                          &chi2, work);
    gsl_multifit_linear_free (work);

    cout << "Goodness of fit is " << chi2 << endl;
    cout << "Results coefficients are" << endl;
    for (int i=0; i<=nPol; i++)
        cout << gsl_vector_get(c,(i)) << " ";
    cout << endl;
    cout << "Covariance matrix is" << endl;
    for (int i=0; i<= nPol; i++)
    {
        string ws;
        stringstream wStr (ws);
        for (int j=0; j<=nPol; j++)
            wStr << gsl_matrix_get(covMatr,(i),(j)) << " ";
        cout << wStr.str() << endl;
    }

    gsl_vector_free (c);
    gsl_vector_free (yVec);
    gsl_matrix_free (covMatr);
    gsl_matrix_free (XMatr);

    delete [] y;
    delete [] coeff0;
    return 0;
}
